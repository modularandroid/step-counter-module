# Step Counter Module

A configurable and customizable Step Counter module ( using Google Fit API ) for android.

*Note that the daily total is computed from midnight of the current day on the device's current timezone. More information here : https://developers.google.com/fit/scenarios/read-daily-step-total*



## Requirements

Before you can use this module you must obtain a client ID from Google Developers. To do this, follow these 3 steps :



1.  Login into your Google account

   

2.   Go here : https://developers.google.com/fit/android/get-api-key 

   - Follow the instructions that correspond to your case ( debug or release ) in the section ***Find your app's certificate information***
   -  Once you get your certificate's SHA-1 fingerprint,  in the section ***Request an OAuth 2.0 client ID in the Google API Console***, click on ***GET A CLIENT ID*** and follow the instructions given in the same section

   

3.  Once you get your client ID, you can continue to follow this documentation



## Installation

From Android Studio, import the library module to your project :

  1. Click File > New > Import Module

  2. Choose **step-counter-module** directory and click Finish. 
     *In case of any import error, just proceed to step 3 and the error should be automatically solved.*

  3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

	```
	include ":app", ":step-counter-module"   
	```

  4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

  ```
  dependencies {
  	implementation project(":step-counter-module")
  }
  ```

  5. Click Sync Project with Gradle Files (Sync Now)



## Usage

This module aims to inflate a LinearLayout contains in your activity .xml layout.

In the activity in which you want to call the module, import it by adding the following line :

```java
import com.mecreativestudio.step_counter_module.StepCounter;
```



Then you can simply use it by inflating the LinearLayout, as follows :

```java
//Find the linearLayout and inflate it with the stepcounter
final LinearLayout YOUR_LINEAR_LAYOUT = findViewById(R.id.YOUR_LINEAR_LAYOUT_ID);

LayoutInflater.from(this).inflate(com.mecreativestudio.step_counter_module.R.layout.step_counter, YOUR_LINEAR_LAYOUT);

//Find the textView of the step-counter-module
final TextView stepCounterValue = findViewById(com.mecreativestudio.step_counter_module.R.id.stepCounterValue);

new StepCounter(this, stepCounterValue);
```

*Note that you must change YOUR_LINEAR_LAYOUT and YOUR_LINEAR_LAYOUT_ID by their respective names.*



### 	Example

Let's say you want to inflate a LinearLayout with the Step Counter module.

In your application, you have an activity called ***MainActivity*** and its associated layout contains a LinearLayout whose id is ***myLinearLayout***.

​	

By following the previous instructions, the code which you should have in your MainActivity.java class is :

```JAVA
import ...
import com.mecreativestudio.step_counter_module.StepCounter;

public class MainActivity extends AppCompatActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Find the linearLayout and inflate it with the stepcounter
        final LinearLayout myLinearLayout = findViewById(R.id.myLinearLayout);
        LayoutInflater.from(this).inflate(com.mecreativestudio.step_counter_module.R.layout.step_counter, myLinearLayout);

        //Find the textView of the step-counter-module
        final TextView stepCounterValue = findViewById(com.mecreativestudio.step_counter_module.R.id.stepCounterValue);

        new StepCounter(this, stepCounterValue);

    }
}
```



## Configuration and Customization

This Step Counter Module is fully configurable and customizable.  

If you want to adapt its behavior to your needs, you can simply edit the StepCounter Java class which is located here :

```
YOUR_PROJECT/step-counter-module/java/com.mecreativestudio.step_counter_module/StepCounter.java
```

*In this case, it is strongly recommended to inform yourself about the use of Google Fit API here : https://developers.google.com/fit/android/*



If you want to edit its layout, you can simply edit the step_counter.xml file which is located here :

```xml
YOUR_PROJECT/step-counter-module/res/layout/step_counter.xml
```
